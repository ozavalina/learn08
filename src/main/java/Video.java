public class Video implements Comparable<Video> {
    String author;
    String name;
    double duration;
    String comment;

    public Video(String author, String name, double duration, String comment) {
        this.author = author;
        this.name = name;
        this.duration = duration;
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Video{" +
                "author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", duration=" + duration +
                ", comment='" + comment + '\'' +
                '}';
    }

    @Override
    public int compareTo(Video o) {
        return Double.compare(this.duration, o.duration);
    }

    @Override
    public int hashCode() {
        return this.author.length() + this.name.length() + (int) this.duration + this.name.length();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (obj.getClass() != getClass()) return false;
        Video video = (Video) obj;
        return this.author.equals(video.author) &&
                this.name.equals(video.name) &&
                this.comment.equals(video.comment) &&
                this.duration == video.duration;
    }
}
