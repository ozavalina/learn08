import java.util.*;

public class Account {
    List<Video> videos;

    public static void main(String[] args) {
        Account account = new Account();
        account.videos = new ArrayList<>();
        account.videos.add(new Video("author1", "video1", 2.5, "comment 1"));
        account.videos.add(new Video("author2", "video2", 1.5, "comment 2"));
        account.videos.add(new Video("author2", "video2", 1.5, "comment 2"));
        account.printAllVideos(account.videos);

        Collections.sort(account.videos);
        account.printAllVideos(account.videos);

        Collections.sort(account.videos, account.videoComparator());
        // не поняла, надо было именно через метод класса делать компоратор или нет, без отдельного метода тоже работает:
        //Collections.sort(account.videos, (o1, o2) -> Double.compare(o1.duration, o2.duration));
        account.printAllVideos(account.videos);

        account.addVideo("author3", "video3", 3.5, "comment 3");
        account.printAllVideos(account.videos);

        account.deleteVideo("video2");
        account.printAllVideos(account.videos);

        account.printAllVideos(account.clearListOfDuplicates());
    }

    public void printAllVideos(List<Video> videos) {
        Iterator<Video> iterator = videos.iterator();
        System.out.println("Вывод с помощью итератора: ");
        while (iterator.hasNext()) {
            Video lc = iterator.next();
            System.out.println(lc);
        }
    }

    public void addVideo(String author, String name, double duration, String comment) {
        videos.add(new Video(author, name, duration, comment));
    }

    public void deleteVideo(String name) {
        Iterator<Video> it = videos.iterator();
        while (it.hasNext()) {
            Video lc = it.next();
            if (lc.name.contains(name)) {
                it.remove();
            }
        }
    }

    public Comparator<Video> videoComparator() {
        return (o1, o2) -> Double.compare(o1.duration, o2.duration);
    }

    public List<Video> clearListOfDuplicates() {
        List<Video> newVideos = new ArrayList<>();
        boolean flag = false;
        for (int i = 0; i < videos.size(); i++) {
            for (int j = i + 1; j < videos.size(); j++) {
                if (videos.get(i).hashCode() == videos.get(j).hashCode()) {
                    if (videos.get(i).equals(videos.get(j))) {
                        flag = true;
                        break;
                    }
                } else {
                    flag = false;
                }
            }
            if (!flag) {
                newVideos.add(videos.get(i));
            }
        }
        return newVideos;
    }
}
