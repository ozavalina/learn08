import java.util.HashMap;
import java.util.Map;

public class Translator {
    //Дополнительно подумай - что надо изменить в коде, что бы на одно английское слово можно было хранить несколько вариантов русского перевода,
    // а не одно слово как сейчас. Код писать не обязательно, можешь просто написать свои мысли.
    //Передавать вместо String value в Map, список:
    //Map<String, ArrayList<String>> map;

    Map<String, String> map;

    public void addElement(String enWord, String ruWord) {
        map.put(enWord, ruWord);
    }

    public String getTranslation(String enWord) {
        return map.get(enWord);
    }

    public void deleteElement(String enWord) {
        map.remove(enWord);
    }

    public static void main(String[] args) {
        Translator translator = new Translator();
        translator.map = new HashMap<>();

        translator.addElement("dictionary", "словарь");
        translator.addElement("choose", "выбирать");
        translator.addElement("eat", "есть");
        System.out.println(translator.map);

        System.out.println(translator.getTranslation("dictionary"));

        translator.deleteElement("choose");
        System.out.println(translator.map);
    }
}
